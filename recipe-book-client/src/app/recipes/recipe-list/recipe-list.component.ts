import { Recipe } from './../recipe.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css'],
})
export class RecipeListComponent {
  public recipes: Recipe[] = [
    new Recipe(
      'A test recipe',
      'this is only a test',
      'https://www.justataste.com/wp-content/uploads/2018/01/broccoli-onions-768x503.jpg'
    ),
    new Recipe(
      'Another test recipe',
      'test description of the recipe',
      'https://www.justataste.com/wp-content/uploads/2018/01/broccoli-onions-768x503.jpg'
    ),
  ];
}
